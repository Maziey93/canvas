
![app_image](./Screen%20Shot%202020-04-07%20at%204.15.16%20PM.png)


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Initial Steps

- You'll need to duplicate the `.env.dist` file and specify the domain for the API URL, there a proxy setup if you'd 
rather not want to deal with cors issues, all you need do is specify a path for `REACT_APP_API_PROXY_URL`  

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

There aren't a lot of them because of the time constraint :(

### Notable

- The app is client side rendered!
