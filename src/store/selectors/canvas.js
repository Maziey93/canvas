import get from 'lodash.get';

export const selectAllCanvases = state => get(state, 'canvas.list', {});

export const selectCurrentCanvasDetails = state => get(state, 'canvas.current', {});
