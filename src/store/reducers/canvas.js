import {
    GET_CANVAS,
    UPDATE_CANVAS,
    CREATE_CANVAS,
    FETCH_CANVASES,
} from '../constants';

/**
 *
 * @param {{}} state
 * @param action
 * @param {string} action.type
 * @param {*} action.payload
 * @returns {{}|{list: *}|{created: *[]}}
 */
const canvas = (state = {}, action = {}) => {
    switch (action.type) {
        case `${FETCH_CANVASES}_FULFILLED`:
            return {...state, list: action.payload};
        case `${FETCH_CANVASES}_FAILED`:
            return {...state, list: { error: true }};
        case `${GET_CANVAS}_FULFILLED`:
        case `${UPDATE_CANVAS}_FULFILLED`:
            return {...state, current: action.payload};
        case CREATE_CANVAS:
            return {...state, created: [...state.created]};
        default:
            return state;
    }
};

export default canvas;
