import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import { shouldFetchCanvases, updateCanvasById  } from './canvas';
import reduxPromiseMiddleware from "redux-promise-middleware";
import {FETCH_CANVASES, UPDATE_CANVAS} from "../constants";

describe('Canvas actions', () => {

    const httpClient =  {
        get: jest.fn(),
        post: jest.fn(),
        put: jest.fn(),
        delete: jest.fn(),
    };

    const middlewares = [
        thunk.withExtraArgument({
            httpClient
        }),
        reduxPromiseMiddleware,
    ];

    const createStore = configureStore(middlewares);


    const baseState =  {
        canvas: {
            list: {}
        }
    };

    describe('shouldFetchCanvases', () => {

        it('makes a request for the canvas if there are no canvases available or forced', async () => {

            const mockedState = {...baseState};

            httpClient.get.mockImplementation(() => Promise.resolve({
                data: [
                    {
                        id: 1,
                        title: 'something',
                        content: [2]
                    },
                ],
                pagination: {},
                link: {}
            }));

            const store = createStore(mockedState);

            const fulfilledAction = await store.dispatch(shouldFetchCanvases({}));

            expect(fulfilledAction.action.type).toBe(`${FETCH_CANVASES}_FULFILLED`);

        });

        it('should not make a request without force if there are canvases in the store', async () => {
            const mockedState = {...baseState, canvas: {
                    list: {
                        data: [
                            {
                                id: 1,
                                title: 'something',
                                content: [2]
                            },
                        ],
                        pagination: {},
                        link: {}
                    }
                }
            };

            const store = createStore(mockedState);

            const fulfilledAction = await store.dispatch(shouldFetchCanvases({}));

            expect(fulfilledAction).toBeNull();
        });
    });


    describe('updateCanvasById', () => {
        it('makes the put request to update the canvas', async () => {
            const updatedContent =  [ 123, 55666, 666];

            const mockedResponse = {
              id: 5,
              title: 'Yoda',
              content: updatedContent,
            };

            httpClient.put.mockImplementationOnce(() => Promise.resolve(mockedResponse));

            const store = createStore(baseState);

            const fulfilledAction = await store.dispatch(updateCanvasById(5, {
                content: updatedContent
            }));

            expect(fulfilledAction.action.type).toBe(`${UPDATE_CANVAS}_FULFILLED`);
        });

    });
});
