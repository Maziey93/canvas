import { FETCH_CANVASES, CREATE_CANVAS, GET_CANVAS, UPDATE_CANVAS } from '../constants';
import { selectAllCanvases } from "../selectors/canvas";

/**
 *
 * @param params
 * @returns {function(*, *, {httpClient: *}): *}
 */
export const fetchCanvases = (params) => (dispatch, getState, { httpClient }) =>
    dispatch({
        type: FETCH_CANVASES,
        payload: httpClient.get({
            url: '/v1/canvases',
            params,
        }),
    });

/**
 *
 * @param params
 * @param {boolean} forceFetch
 * @returns {function(*, *): *}
 */
export const shouldFetchCanvases = (params, forceFetch = false) => (dispatch, getState) => {
    const allCanvases = selectAllCanvases(getState());

    if(Object.keys(allCanvases).length && !forceFetch) return null;

    return dispatch(fetchCanvases(params));
};

/**
 *
 * @param param
 * @returns {function(*, *, {httpClient: *}): *}
 */
export const createCanvas = (param) => (dispatch, getState, { httpClient }) =>
    dispatch({
        type: CREATE_CANVAS,
        payload: httpClient.post({
            url: '/v1/canvases',
        }),
    });

/**
 *
 * @param {number} id
 * @returns {function(*, *, {httpClient: *}): *}
 */
export const getCanvasById = id => (dispatch, getState, { httpClient }) =>
    dispatch({
        type: GET_CANVAS,
        payload: httpClient.get({
            url: `/v1/canvases/${id}`,
        }),
    });

/**
 *
 * @param {number} id
 * @param {object} body
 * @returns {function(*, *, {httpClient: *}): *}
 */
export const updateCanvasById = (id, body) => (dispatch, getState, { httpClient }) =>
    dispatch({
        type: UPDATE_CANVAS,
        payload: httpClient.put({
            url: `/v1/canvases/${id}`,
            data: body,
        }),
    });
