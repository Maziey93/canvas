import thunk from 'redux-thunk';
import { applyMiddleware, createStore, compose } from 'redux';
import reduxPromiseMiddleware from 'redux-promise-middleware';
import rootReducer from './reducers';
import HttpClient from "../lib/httpClient";
import config from '../config';

/**
 *
 * @param {{}} [initialState]
 * @returns {Store<CombinedState<unknown> & {}, AnyAction> & Store<S & {}, A> & {dispatch: unknown}}
 */
const configureStore = (initialState = {}) => {

    const httpClient = new HttpClient(config.api);

    const middlewares = [
        thunk.withExtraArgument({
            httpClient,
        }),
        reduxPromiseMiddleware,
    ];

    return createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(...middlewares),
            typeof window != 'undefined' &&
            typeof window.__REDUX_DEVTOOLS_EXTENSION__ === 'function'
                ? window.__REDUX_DEVTOOLS_EXTENSION__({ name: 'canvas-demo' })
                : f => f
        )
    );
};

export default configureStore;
