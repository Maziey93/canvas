const { createProxyMiddleware } = require('http-proxy-middleware');
import { api } from './config';

module.exports = function(app){

    if(api.proxyBaseUrl) {
        app.use(
            [api.proxyBaseUrl],
            createProxyMiddleware({
                target: api.url,
                changeOrigin: true,
                logLevel: 'debug',
                pathRewrite: {
                    [`^/${api.proxyBaseUrl}/v1`]: '/v1'
                }
            })
        );
    }
};
