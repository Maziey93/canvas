import HttpClient from "./httpClient";

describe('Http Client', () => {

    const config = {
        baseUrl: '/api',
        timeout: 2000
    };

    it('it should return an instance of itself', () => {
        expect(new HttpClient(config)).toBeInstanceOf(HttpClient);
    });

    it('it should return an instance with the common HTTP verbs', () => {
        const httpClient = new HttpClient(config);
        expect(httpClient).toHaveProperty('get');
        expect(httpClient).toHaveProperty('post');
        expect(httpClient).toHaveProperty('delete');
        expect(httpClient).toHaveProperty('put');
    });
});
