/**
 *
 * @param int
 * @returns {string}
 */
export const deserialize = (int) => parseInt(int, 10).toString(2);

export const serializeRow = (row) => {
    // normalize rows that might have been empty
    let binNormalized = [];

    for(let i = 0; i < row.length; ++i) {
        let val = row[i];

        binNormalized.push(Boolean(val) ? 1: 0);
    }

    return parseInt(binNormalized.join(''), 2);
};
