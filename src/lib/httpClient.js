import axios from 'axios';


class HttpClient {
    /**
     *
     * @param apiConfig
     * @param {string} apiConfig.url
     * @param {number} apiConfig.timeout
     * @param {number} apiConfig.proxyBaseUrl
     */
    constructor(apiConfig) {
        this.transport = axios.create({
            baseURL: apiConfig.proxyBaseUrl || apiConfig.url,
            timeout: apiConfig.timeout,
        });

        this.transport.interceptors.response.use(
            this.responseSuccessHandler.bind(this),
            this.responseErrorHandler.bind(this)
        );

        this.exposeClientMethods();
    }

    responseSuccessHandler(response) {
        return response.data;
    }

    responseErrorHandler(error) {
        if (error.response) {
            const {
                response: { data, status },
            } = error;

            return Promise.reject({ data, status });
        } else {
            return Promise.reject(error);
        }
    }

    requestHandler(method, requestObject) {
        return this.transport({
            ...requestObject,
            method,
        });
    }

    exposeClientMethods() {
        ['get', 'post', 'put', 'patch', 'delete'].forEach(method => {
            this[method] = (arg = {}) => this.requestHandler(method, arg);
        });
    }
}

export default HttpClient;
