import React, { useEffect } from 'react';
import { Link } from "react-router-dom";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { shouldFetchCanvases } from '../../store/actions/canvas';
import { selectAllCanvases } from '../../store/selectors/canvas';
import CanvasThumbnailListItem from "../../components/CanvasThumbnailListItem";
import './style.scss';


const CanvasListView = () => {
    const dispatch = useDispatch();
    const canvasData = useSelector(selectAllCanvases);

    useEffect(() => {
        dispatch(shouldFetchCanvases({}));
    }, [dispatch]);

    const {data: allCanvases  } = canvasData;

    return (
        <div className="">
            <div className="canvas-list__wrapper">
                {Object.keys(canvasData).length ?
                    (
                        <div className="canvas-list">
                            {allCanvases.map((canvas) => (
                                <div className="canvas-list__item" key={canvas.id}>
                                    <Link to={`/canvas/${canvas.id}`}>
                                        <CanvasThumbnailListItem {...canvas} preview={true} />
                                    </Link>
                                </div>
                            ))}
                        </div>
                    )
                    : <div>Loading...</div>
                }
            </div>

            {/* TODO: come back to this is there's enough time */}
            <div className="pagination"></div>
        </div>
    );
};

export default CanvasListView;
