import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import CanvasThumbnailListItem from "../../components/CanvasThumbnailListItem";
import { getCanvasById } from '../../store/actions/canvas';
import { selectCurrentCanvasDetails } from '../../store/selectors/canvas';
import './canvas-single-view-style.scss';

const CanvasSingleView = () => {
    const dispatch = useDispatch();
    const { canvasId } = useParams();
    const canvas = useSelector(selectCurrentCanvasDetails, shallowEqual);

    useEffect(() => {
       if(canvasId){
           dispatch(getCanvasById(canvasId));
       }
    }, [dispatch, canvasId]);

    return(
        <div className="canvas-single-view">
            <div className="canvas-single-view__back-link">
                <Link to="/">&#x2190; Back</Link>
            </div>
            {canvas && canvas.id ? (
                <div className="">
                    <CanvasThumbnailListItem size={[16, 16]} {...canvas}/>
                </div>
            ) :
                <div data-test-id="loadingElm">Loading...</div>
            }
        </div>
    )
};


export default CanvasSingleView;
