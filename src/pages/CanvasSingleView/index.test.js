import React from 'react';
import { shallow } from "enzyme";
import { MemoryRouter } from 'react-router-dom';
import { Provider, useSelector } from 'react-redux';
import configureStore from "redux-mock-store";
import CanvasSingleView from "./index";

const mockCanvasId = 5;

jest.mock('react-redux', () => ({
    ...jest.requireActual('react-redux'),
    useSelector: jest.fn(),
    useDispatch: jest.fn(() => () => {})
}));

jest.mock('react-router', () => ({
    ...jest.requireActual('react-router'),
    useParams: jest.fn(() => ({
        mockCanvasId,
    }))
}));

describe('CanvasSingleView', () => {
    const createStore = configureStore();

    const renderComponent = (props = {}, state = {}) => shallow(
        <MemoryRouter>
            <Provider store={createStore(state)}>
                <CanvasSingleView {...props} />
            </Provider>
        </MemoryRouter>
    );

    it('renders without crashing', () => {
        const selectors = {
            selectCurrentCanvasDetails: {
                id: mockCanvasId,
                title: '',
                content: [],
            }
        };

        useSelector.mockImplementationOnce(selector => selectors[selector.name]);

        const renderedComponent = renderComponent();

        expect(renderedComponent).toMatchSnapshot();
        expect(renderedComponent.dive().find(CanvasSingleView).exists()).toBe(true);

    });
});
