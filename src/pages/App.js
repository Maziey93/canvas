import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import CanvasListView from "./CanvasListView";
import CanvasSingleView from "./CanvasSingleView";
import './App.scss';

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <h1>Canvas Playground</h1>
      </header>
      <main className="App-content">
        <Router>
            <Switch>
                <Route path={'/'} component={CanvasListView} exact={true}/>
                <Route path={'/canvas/:canvasId'} component={CanvasSingleView} exact={true}/>
                <Route path={'*'} render={() => 'nothing to see here'}/>
            </Switch>
        </Router>
      </main>
    </div>
  );
}

export default App;
