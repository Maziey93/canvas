import React from 'react';
import CanvasGrid from "../CanvasGrid";
import './style.scss'

/**
 *
 * @param id
 * @param title
 * @param content
 * @param preview
 * @param size
 * @returns {*}
 * @constructor
 */
const CanvasThumbnailListItem = ({id, title, content, preview = false, size = [16, 16], }) => {
    return (
        <div className="canvas-thumbnail" data-test-id={id}>
            <CanvasGrid canvasId={id} size={size} content={content} disabled={preview}/>
            <h3>{title}</h3>
        </div>
    );
};

export default CanvasThumbnailListItem;
