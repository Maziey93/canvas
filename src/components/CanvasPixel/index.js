import React, { useEffect, useState, useRef } from 'react';
import './canvas-pixel-styles.scss';

/**
 *
 * @param idx
 * @param {array} RowPixels
 * @param {number} rowId
 * @param {function} updater
 * @returns {*}
 */
const CanvasPixel = ({ idx, rowPixels, rowId, updater, disabled }) => {

    const [isActive, setIsActive ] = useState(Boolean(rowPixels[idx]));
    const hasPerformedFirstClick = useRef(false);

    useEffect(() => {
        if(hasPerformedFirstClick.current) {

            const update = rowPixels.slice(0);

            update[idx] =  Boolean(isActive) ? 1 : 0;

            // call helper function that is used to update the state of the canvas
            updater({ rowId, updatedRow: update});
        }
    }, [isActive, idx, rowId, rowPixels, updater]);

    const handleSelection = (evt) =>  {
        evt.preventDefault();

        if(!hasPerformedFirstClick.current) {
            hasPerformedFirstClick.current = true;
        }

        setIsActive(active => !active);
    };

    return (
        <li
            onClick={handleSelection}
            className={`canvas-pixel ${isActive && "canvas-pixel--active"} ${disabled && "canvas-pixel--disabled"}`}>
                <span key={idx} className=""/>
        </li>
    );
};


export default CanvasPixel;
