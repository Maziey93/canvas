import React from 'react';
import { useDispatch,  } from 'react-redux';
import CanvasPixel from "../CanvasPixel";
import { deserialize } from '../../lib/utils';
import { updateCanvasById } from '../../store/actions/canvas';
import { serializeRow } from "../../lib/utils";
import './canvas-grid-styles.scss';

export const iterateAndGeneratePixel = (times, Component, props = []) =>
    Array.from(new Array(times))
        .map((_, idx) => {
            return <Component key={idx} idx={idx} injectedProp={props[idx]}/>
        });

/**
 * @param {object} props
 * @param {[number, number]} props.size
 * @param {number} props.canvasId
 * @param {array} props.content
 * @param {boolean} props.disabled
 */
const CanvasGrid = ({canvasId, size, content , disabled}) => {
    const [length, breadth] = size;

    const dispatch = useDispatch();

    const updatePixel = ({ rowId, updatedRow}) =>  {
        if(disabled) return;

        const serializedChunk = serializeRow(updatedRow);

        // Make copy to ensure we dont have race condition messing up the data
        const updateToSync = content.slice(0);

        updateToSync[rowId] = serializedChunk;

        //Perform update Magic here
        return dispatch(updateCanvasById(canvasId, { content: [...updateToSync] }));
    };

    return (
        <div className="canvas-grid">
            {iterateAndGeneratePixel(length, ({idx: rowId, injectedProp}) => {
                const pixelMarking = String(deserialize(injectedProp)).split("");

                return (
                    <ul className="canvas-grid__row" key={rowId}>
                        {iterateAndGeneratePixel(breadth, ({ idx }) => {
                            return (
                                <CanvasPixel
                                    key={idx}
                                    idx={idx}
                                    rowId={rowId}
                                    rowPixels={pixelMarking}
                                    disabled={disabled}
                                    updater={updatePixel} />
                                );
                        })}
                    </ul>
                );
            }, content)}
        </div>
    );
};


export default CanvasGrid;
