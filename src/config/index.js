module.exports = {
    env: process.env.NODE_ENV || 'development',
    api: {
        url: process.env.REACT_APP_API_URL,
        proxyBaseUrl: process.env.REACT_APP_API_PROXY_URL,
        timeout: parseInt(process.env.REACT_APP_API_TIMEOUT, 10),
    },
};
